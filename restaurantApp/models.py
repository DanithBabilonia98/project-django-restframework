from django.db import models


# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)


class Waiter(Person):
    last_name_2 = models.CharField(max_length=45)

    class Meta:
        verbose_name = 'Waiter'

    def __str__(self):
        return f'{self.name} {self.last_name} {self.last_name_2}'


class Customer(Person):
    observations = models.TextField()

    class Meta:
        verbose_name = 'Customer'

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Table(models.Model):
    number_customers = models.IntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return f'{self.location} - {self.number_customers}'


class Product(models.Model):
    name = models.CharField(max_length=45)
    price = models.FloatField()
    types = [('food', 'Food'), ('drink', 'Drink')]
    type = models.CharField(max_length=5, choices=types)

    def __str__(self):
        return f'{self.name} - {self.price}'


class Order(models.Model):
    id_product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return f'{self.id_product} - {self.quantity}'


class Invoice(models.Model):
    id_customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    id_waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE)
    id_order = models.ForeignKey(Order, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    total = models.FloatField()

    def __str__(self):
        return f'{self.id_customer} - {self.id_waiter} - {self.id_table} - {self.id_order} - {self.date} - {self.total}'
