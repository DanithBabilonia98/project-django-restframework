from django.contrib import admin
from .models import Waiter, Customer, Table, Product, Order, Invoice
# Register your models here.

admin.site.register(Customer)
admin.site.register(Waiter)
admin.site.register(Table)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(Invoice)