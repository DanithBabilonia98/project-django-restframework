from django.urls import path
from rest_framework.routers import SimpleRouter

from restaurantApp.views import HomeView, CustomerViewSet, WaiterViewSet, ProductViewSet, TableViewSet, OrderViewSet, \
    InvoiceViewSet, CustomerCreateView, CustomerList, CustomerUpdateView, CustomerCanceled, \
    WaiterCreateView, WaiterList,WaiterUpdateView,WaiterCanceled,\
         ProductCreateView, ProductList, ProductUpdateView, ProductCanceled, \
            TableCreateView, TableList, TableUpdateView, TableCanceled, \
            OrderCreateView, OrderList, OrderUpdateView, OrderCanceled, \
            InvoiceCreateView, InvoiceList, InvoiceUpdateView, InvoiceCanceled

router = SimpleRouter()
router.register(r'customers', CustomerViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'waiters', WaiterViewSet)
router.register(r'products', ProductViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'tables', TableViewSet)

app_name = 'restaurantApp'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('customer_register/', CustomerCreateView.as_view(), name='customer_register'),
    path('customer_list/', CustomerList.as_view(), name='customer_list'),
    path('customer_update/<int:pk>/', CustomerUpdateView.as_view(), name='customer_update'),
    path('customer_cancel/<int:pk>/', CustomerCanceled.as_view(), name='customer_cancel'),

    path('waiter_register/', WaiterCreateView.as_view(), name='waiter_register'),
    path('waiter_list/', WaiterList.as_view(), name='waiter_list'),
    path('waiter_update/<int:pk>/', WaiterUpdateView.as_view(), name='waiter_update'),
    path('waiter_cancel/<int:pk>/', WaiterCanceled.as_view(), name='waiter_cancel'),

    path('product_register/', ProductCreateView.as_view(), name='product_register'),
    path('product_list/', ProductList.as_view(), name='product_list'),
    path('product_update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('product_cancel/<int:pk>/', ProductCanceled.as_view(), name='product_cancel'),

    path('table_register/', TableCreateView.as_view(), name='table_register'),
    path('table_list/', TableList.as_view(), name='table_list'),
    path('table_update/<int:pk>/', TableUpdateView.as_view(), name='table_update'),
    path('table_cancel/<int:pk>/', TableCanceled.as_view(), name='table_cancel'),

    path('order_register/', OrderCreateView.as_view(), name='order_register'),
    path('order_list/', OrderList.as_view(), name='order_list'),
    path('order_update/<int:pk>/', OrderUpdateView.as_view(), name='order_update'),
    path('order_cancel/<int:pk>/', OrderCanceled.as_view(), name='order_cancel'),

    path('invoice_register/', InvoiceCreateView.as_view(), name='invoice_register'),
    path('invoice_list/', InvoiceList.as_view(), name='invoice_list'),
    path('invoice_update/<int:pk>/', InvoiceUpdateView.as_view(), name='invoice_update'),
    path('invoice_cancel/<int:pk>/', InvoiceCanceled.as_view(), name='invoice_cancel'),
] + router.urls



