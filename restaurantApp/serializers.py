from rest_framework.serializers import ModelSerializer
from restaurantApp.models import Order, Customer, Product, Table, Waiter, Invoice


class CustomerSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class TableSerializer(ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'


class WaiterSerializer(ModelSerializer):
    class Meta:
        model = Waiter
        fields = '__all__'


class InvoiceSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'