from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from rest_framework import viewsets, permissions

from .serializers import CustomerSerializer, InvoiceSerializer, OrderSerializer, TableSerializer, ProductSerializer, \
    WaiterSerializer
from .forms import CustomerForm, WaiterForm, ProductForm, TableForm, InvoiceForm, OrderForm
from .models import Customer, Order, Product, Table, Invoice, Waiter


# Create your views here.
class HomeView(TemplateView):
    template_name = 'home.html'



# CRUD Customers

class CustomerCreateView(CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customer/create_customer.html'
    success_url = '/'


class CustomerList(ListView):
    model = Customer
    queryset = Customer.objects.all()
    template_name = 'customer/list_custom.html'
    context_object_name = 'Customer'


class CustomerUpdateView(UpdateView):
    model = Customer
    form_class = CustomerForm
    pk_url_kwargs = 'pk'
    template_name = 'customer/update_customer.html'
    success_url = '/'


class CustomerCanceled(DeleteView):
    model = Customer
    pk_url_kwargs = 'pk'
    template_name = 'customer/delete_customer.html'
    success_url = '/'


# CRUD Waiters

class WaiterCreateView(CreateView):
    model = Waiter
    form_class = WaiterForm
    template_name = 'waiter/create_waiter.html'
    success_url = '/'


class WaiterList(ListView):
    model = Waiter
    queryset = Waiter.objects.all()
    template_name = 'waiter/list_waiter.html'
    context_object_name = 'waiter'


class WaiterUpdateView(UpdateView):
    model = Waiter
    form_class = WaiterForm
    pk_url_kwargs = 'pk'
    template_name = 'waiter/update_waiter.html'
    success_url = '/'


class WaiterCanceled(DeleteView):
    model = Waiter
    pk_url_kwargs = 'pk'
    template_name = 'waiter/delete_waiter.html'
    success_url = '/'


# CRUD Products

class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'product/create_Product.html'
    success_url = '/'


class ProductList(ListView):
    model = Product
    queryset = Product.objects.all()
    template_name = 'product/list_Product.html'
    context_object_name = 'Product'


class ProductUpdateView(UpdateView):
    model = Product
    form_class = ProductForm
    pk_url_kwargs = 'pk'
    template_name = 'product/update_Product.html'
    success_url = '/'


class ProductCanceled(DeleteView):
    model = Product
    pk_url_kwargs = 'pk'
    template_name = 'product/delete_Product.html'
    success_url = '/'


# CRUD Invoices

class InvoiceCreateView(CreateView):
    model = Invoice
    form_class = InvoiceForm
    template_name = 'invoice/create_invoice.html'
    success_url = '/'


class InvoiceList(ListView):
    model = Invoice
    queryset = Invoice.objects.all()
    template_name = 'invoice/list_Invoice.html'
    context_object_name = 'invoice'


class InvoiceUpdateView(UpdateView):
    model = Invoice
    form_class = InvoiceForm
    pk_url_kwargs = 'pk'
    template_name = 'invoice/update_Invoice.html'
    success_url = '/'


class InvoiceCanceled(DeleteView):
    model = Invoice
    pk_url_kwargs = 'pk'
    template_name = 'invoice/delete_Invoice.html'
    success_url = '/'


# CRUD Tables

class TableCreateView(CreateView):
    model = Table
    form_class = TableForm
    template_name = 'table/create_Table.html'
    success_url = '/'


class TableList(ListView):
    model = Table
    queryset = Table.objects.all()
    template_name = 'table/list_Table.html'
    context_object_name = 'table'


class TableUpdateView(UpdateView):
    model = Table
    form_class = TableForm
    pk_url_kwargs = 'pk'
    template_name = 'table/update_Table.html'
    success_url = '/'


class TableCanceled(DeleteView):
    model = Table
    pk_url_kwargs = 'pk'
    template_name = 'table/delete_Table.html'
    success_url = '/'


# CRUD Orders

class OrderCreateView(CreateView):
    model = Order
    form_class = OrderForm
    template_name = 'order/create_Order.html'
    success_url = '/'


class OrderList(ListView):
    model = Order
    queryset = Order.objects.all()
    template_name = 'order/list_Order.html'
    context_object_name = 'Order'


class OrderUpdateView(UpdateView):
    model = Order
    form_class = OrderForm
    pk_url_kwargs = 'pk'
    template_name = 'order/update_Order.html'
    success_url = '/'


class OrderCanceled(DeleteView):
    model = Order
    pk_url_kwargs = 'pk'
    template_name = 'order/delete_Order.html'
    success_url = '/'


# Views Serializers
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [permissions.IsAuthenticated]


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
