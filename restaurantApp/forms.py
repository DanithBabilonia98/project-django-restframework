from django import forms


# Form Customer
from restaurantApp.models import Order, Invoice, Table, Product, Customer, Waiter


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ['name', 'last_name', 'observations']


# Form Waiter

class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ['name', 'last_name', 'last_name_2']


# Form Table

class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = '__all__'


# Form Product

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = "__all__"


# Form Order
class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = "__all__"


# Form Table
class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = "__all__"


# Form Invoice
class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = "__all__"
